// compound assignment operators
#include <iostream>
using namespace std;
const char newline = '\n';

int main ()
{
	std::cout << "Was soll berechnet werden?" << newline;
	std::cout << "1. Grundwert" << newline;
	std::cout << "2. Prozentwert" << newline;	
	std::cout << "3. Prozentsatz" << newline;

	int n;
	std::cin >> n;
	// std::cout << n;

	// variables prozent
	double W; // prozentwert W
	double p; // prozentsatz p %
	double G; // Grundwert G

	switch (n)
	{
		case 1:
			std::cout << "Prozentwert W:" << newline;
			std::cin >> W;
			std::cout << newline;
			std::cout << "Prozentsatz p %:" << newline;
			std::cin >> p;
			std::cout << newline;
			G = W * 100 / p;
			std::cout << "Grundwert G: " << G << newline;
			break;

		case 2:
			std::cout << "Prozentsatz p %:" << newline;
			std::cin >> p;
			std::cout << newline;
			std::cout << "Grundwert G:" << newline;
			std::cin >> G;
			std::cout << newline;
			W = p * G / 100;
			std::cout << "Prozentwert W: " << W << newline;
			break;

		case 3:	
			std::cout << "Prozentwert W:" << newline;
			std::cin >> W;
			std::cout << newline;
			std::cout << "Grundwert G:" << newline;
			std::cin >> G;
			std::cout << newline;
			p = W * 100 / G;
			std::cout << "Prozentsatz p %: " << p << newline;
			break;

		default:
			std::cout << "Fehler bei Eingabe." << newline;
			break;
	}

	return 0;
}
